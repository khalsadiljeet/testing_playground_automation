package com.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.base.BasePage;

/*This class contains all the web locators & applicable methods for 
Visibility page*/
public class Visibility extends BasePage {
	
	private WebDriver driver;
	
	@FindBy(id = "hideButton")
	WebElement hideButton;
	
	@FindBy(id="removedButton")
	WebElement removedButton;
	
	@FindBy(id="zeroWidthButton")
	WebElement zeroWidthButton;
	
	@FindBy(id="overlappedButton")
	WebElement overlappedButton;
	
	@FindBy(id="transparentButton")
	WebElement opacityButton;
	
	@FindBy(id="invisibleButton")
	WebElement visibilityHiddenButton;
	
	@FindBy(id="notdisplayedButton")
	WebElement displayNoneButton;

	@FindBy(id="offscreenButton")
	WebElement offscreenButton;
	
	public Visibility(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	
	public void clickHideButton(){
		clickOnElement(hideButton);
	}
	
	public boolean verifyRemovedButtonIsHidden(){
        boolean flag = false;
        try {
        	waitForElementExistance(removedButton);
			flag = false;
		}catch(NoSuchElementException e) {
			flag = true;
		}
       return flag;
	}
	
	public boolean verifyZeroWidthButtonIsHidden(){
		boolean flag = false;
        try {
        	waitForElementExistance(zeroWidthButton);
			flag = false;
		}catch(NoSuchElementException e) {
			flag = true;
		}catch(TimeoutException e) {
			flag = true;
		}
       return flag;
	}
	
	public boolean verifyOverLappedButtonIsHidden(){
		boolean flag = false;
        try {
        	 waitForElementExistance(overlappedButton);
			flag = false;
		}catch(NoSuchElementException e) {
			flag = true;
		}
       return flag;
	}
	
	public boolean verifyOpacityButtonIsHidden(){
		boolean flag = false;
		if(opacityButton.getAttribute("style").equals("visibility: hidden;") || opacityButton.getAttribute("style").equals("display: none;")
				|| opacityButton.getAttribute("style").equals("opacity: 0;") || opacityButton.getText().equals("Offscreen")) {
			flag = true;
		}
		
		return flag;
	}

	public boolean verifyVisibilityHiddenButtonIsHidden(){
		boolean flag = false;
		if(visibilityHiddenButton.getAttribute("style").equals("visibility: hidden;") || visibilityHiddenButton.getAttribute("style").equals("display: none;")
				|| visibilityHiddenButton.getAttribute("style").equals("opacity: 0;") || visibilityHiddenButton.getText().equals("Offscreen")) {
			flag = true;
		}
		
		return flag;
	}
	
	public boolean verifyDisplayNoneButtonIsHidden(){
		boolean flag = false;
		if(displayNoneButton.getAttribute("style").equals("visibility: hidden;") || displayNoneButton.getAttribute("style").equals("display: none;")
				|| displayNoneButton.getAttribute("style").equals("opacity: 0;") || displayNoneButton.getText().equals("Offscreen")) {
			flag = true;
		}
		
		return flag;
	}
	
	public boolean verifyOffscreenButtonIsHidden(){
		boolean flag = false;
        try {
        	waitForElementExistance(offscreenButton);
			flag = false;
		}catch(NoSuchElementException e) {
			flag = true;
		}catch(TimeoutException e) {
			flag = true;
		}
       return flag;
	}
	
	

}
