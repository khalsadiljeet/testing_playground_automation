package com.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.base.BasePage;

/*This class contains all the web locators & applicable methods for 
Class Attribute page*/
public class ClassAttribute extends BasePage {
	
	private WebDriver driver;
	@FindBy(xpath = "//button[contains(concat(' ', normalize-space(@class), ' '), ' btn-primary ')]")
	WebElement button;
	Alert alert;
	
	public ClassAttribute(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	public void clickButton() {
		clickOnElement(button);
	}
	
	public void switchToAlert() {
		try {
			waitForAlertToBePresent();
		    alert = driver.switchTo().alert();
		} catch (Exception e) {
			e.printStackTrace();   
		}
		
	}
	
	public String getAlertText() {
		return alert.getText();
	}

	public void clickOkOnAlert() {
		alert.accept();
	}
}
