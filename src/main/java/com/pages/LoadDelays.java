package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.base.BasePage;

/*This class contains all the web locators & applicable methods for 
Load Delays page*/
public class LoadDelays extends BasePage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//*[contains(@class, 'btn-primary')]")
	WebElement buttonAppearingAfterDelay;
	
	public LoadDelays(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	public void clickButtonAppearingAfterDelay(){
		clickOnElement(buttonAppearingAfterDelay);
	}
	
	

}
