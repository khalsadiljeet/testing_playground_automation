package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.base.BasePage;

/*This class contains all the web locators & applicable methods for 
Home page*/
public class Home extends BasePage {
	private WebDriver driver;
	
	@FindBy(xpath = "//a[contains(text(),'Dynamic ID')]")
	WebElement dynamicId;

	@FindBy(xpath = "//a[contains(text(),'Class Attribute')]")
	WebElement classAttribute;
	
	@FindBy(xpath = "//a[contains(text(),'Hidden Layers')]")
	WebElement hiddenLayers;
	

	@FindBy(xpath = "//a[contains(text(),'Load Delay')]")
	WebElement loadDelay;

	@FindBy(xpath = "//a[contains(text(),'AJAX Data')]")
	WebElement ajaxData;
	
	@FindBy(xpath = "//a[contains(text(),'Client Side Delay')]")
	WebElement clientSideDelay;
	
	@FindBy(xpath = "//a[contains(text(),'Click')]")
	WebElement clickLink;
	
	@FindBy(xpath = "//a[contains(text(),'Text Input')]")
	WebElement textInput;
	
	@FindBy(xpath = "//a[contains(text(),'Scrollbars')]")
	WebElement scrollbars;
	
	@FindBy(xpath = "//a[contains(text(),'Dynamic Table')]")
	WebElement dynamicTable;
	
	@FindBy(xpath = "//a[contains(text(),'Verify Text')]")
	WebElement verifyText;
	
	@FindBy(xpath = "//a[contains(text(),'Progress Bar')]")
	WebElement progressBar;

	@FindBy(xpath = "//a[contains(text(),'Visibility')]")
	WebElement visibility;
	
	@FindBy(xpath = "//a[contains(text(),'Sample App')]")
	WebElement sampleApp;
	
	@FindBy(xpath = "//a[contains(text(),'Mouse Over')]")
	WebElement mouseOver;
	
	@FindBy(xpath = "//a[contains(text(),'Non-Breaking Space')]")
	WebElement nonBreakingSpace;
	
	@FindBy(xpath = "//a[contains(text(),'Overlapped Element')]")
	WebElement overlappedElement;
	
	@FindBy(xpath = "//a[contains(text(),'Shadow DOM')]")
	WebElement shadowDom;

	public Home(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	public String verifyHomePageTitle(){
		return driver.getTitle();
	}
	
	public void clickOnDynamicIDLink(){
		clickOnElement(dynamicId);
		
	}
	
	public void clickOnClassAttributeLink(){
		clickOnElement(classAttribute);
	}
	
	public void clickOnHiddenLayersLink(){
		clickOnElement(hiddenLayers);
	}

	public void clickOnLoadDelayLink(){
		clickOnElement(loadDelay);
	}

	public void clickOnAJAXDataLink(){
		clickOnElement(ajaxData);
	}

	public void clickOnClientSideDelayLink(){
		clickOnElement(clientSideDelay);
	}
	
	public void clickOnClickLink(){
		clickOnElement(clickLink);
	}

	public void clickOnTextInputLink(){
		clickOnElement(textInput);
	}
	
	public void clickOnScrollbarsLink(){
		clickOnElement(scrollbars);
	}
	
	public void clickOnDynamicTableLink(){
		//dynamicTable.click();
		clickOnElement(dynamicTable);
	}

	public void clickOnVerifyTextLink(){
		clickOnElement(verifyText);
	}

	public void clickOnProgessBarLink(){
		clickOnElement(progressBar);
	}
	
	public void clickOnVisibilityLink(){
		clickOnElement(visibility);
	}

	public void clickOnSampleAppLink(){
		clickOnElement(sampleApp);
	}
	
	public void clickOnMouseOverLink(){
		clickOnElement(mouseOver);
	}
	
	public void clickOnNonBreakingSpaceLink(){
		clickOnElement(nonBreakingSpace);
	}
	
	public void clickOnOverlappedElementLink(){
		clickOnElement(overlappedElement);
	}
	
	public void clickOnShadowDOMLink(){
		clickOnElement(shadowDom);	
	}
}
