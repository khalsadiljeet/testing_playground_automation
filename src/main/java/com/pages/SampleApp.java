package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.base.BasePage;

/*This class contains all the web locators & applicable methods for 
Sample App page*/
public class SampleApp extends BasePage {
	private WebDriver driver;
	
	@FindBy(id = "loginstatus")
	WebElement loginStatus;
	
	@FindBy(name="UserName")
	WebElement UsernameTextbox;
	
	@FindBy(name="Password")
	WebElement PasswordTextbox;
	
	@FindBy(id="login")
	WebElement loginButton;
	
	public SampleApp(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	public void clickLoginButton(){
		clickOnElement(loginButton);
	}
	
	public String getLoginStatusText(){
		String text;
		waitForElementExistance(loginStatus);
		text = loginStatus.getText();
		return text;
	}
	
	public void enterUserName(String userName){
		setData(UsernameTextbox, userName);
	}
	
	public void enterPassword(String password){
		setData(PasswordTextbox, password);
	}

}
