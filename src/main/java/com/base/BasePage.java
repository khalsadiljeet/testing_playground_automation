package com.base;

import java.time.Duration;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

/*
 * This is BasePage class, which has generic function to interact with Web
 * Elements
 */
public class BasePage {
	protected WebDriver driver;
	protected Wait<WebDriver> wait;

	/*
	 * Instantiates a new base page with fluent wait
	 */
	public BasePage(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new FluentWait<WebDriver>(this.driver)
				.withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(5));
	}

	/*
	 * Wait for Element Existance
	 */
	public void waitForElementExistance(WebElement we) {
		wait.until(ExpectedConditions.visibilityOf(we));
	}

	/*
	 * Wait for Element Clickable
	 */
	public void waitForElementClickable(WebElement we) {
		wait.until(ExpectedConditions.elementToBeClickable(we));
	}
	
	/*
	 * Wait for Alert to be present
	 */
	public void waitForAlertToBePresent() {
		wait.until(ExpectedConditions.alertIsPresent());
	}
	
	/*
	 * Wait for page load
	 */
	public void waitForPageLoad() {
		  JavascriptExecutor j = (JavascriptExecutor) driver;
	      j.executeScript("return document.readyState")
	      .toString().equals("complete");
	}

	/*
	 * To Set data into input box after clearing the existing values
	 */
	public void setData(WebElement weInput, String sValue) {
		waitForElementExistance(weInput);
		weInput.clear();
		weInput.sendKeys(sValue);
	}

	/*
	 * To click on Element 
	 */
	public void clickOnElement(WebElement weInput) {
		try {
			waitForElementClickable(weInput);
			weInput.click();
		}catch(Exception e) {
			clickOnElementJSE(weInput);
		}
	}
	
	/*
	 * To click on Element using Javascript Executor syntax
	 */
	public void clickOnElementJSE(WebElement weInput) {
			JavascriptExecutor jse= (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click();", weInput);
	}
	
	/*
	 * To check if WebElement exists or not
	 */
	public boolean checkWebElementExist(WebElement we) {
		boolean bFlag = false;
		try {
			waitForElementExistance(we);
			bFlag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bFlag;
	}
}
