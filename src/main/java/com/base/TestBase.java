package com.base;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import com.utils.ConfigManager;
import io.github.bonigarcia.wdm.WebDriverManager;

/*This is TestBase class where based on browser choice, browser initialisation/settings & login 
to URL is done*/
public class TestBase {
	
	public static WebDriver driver;
	public static Properties prop;
	public static String url = ConfigManager.getInstance().getString("url");
	public static String browserName = ConfigManager.getInstance().getString("browser");

	
	public static void initialization() throws IOException{
		
        try {
			switch(browserName.toLowerCase()) {
			
			case "chrome": {
			    	// Using WebDriverManager as it removes the chromedriver version dependency
			    	WebDriverManager.chromedriver().setup();
			    	ChromeOptions options = new ChromeOptions();
			    	options.setPageLoadStrategy(PageLoadStrategy.NONE); 
			    	options.addArguments("start-maximized"); 
			    	//options.addArguments("--headless"); 
			    	options.addArguments("enable-automation"); 
			    	options.addArguments("--no-sandbox");
			    	options.addArguments("--disable-infobars"); 
			    	options.addArguments("--disable-dev-shm-usage"); 
					options.addArguments("--disable-browser-side-navigation"); 
					options.addArguments("--disable-gpu");
					driver = new ChromeDriver(options);
					break;
			     }
			    
			    case "firefox": {
			    	// Using WebDriverManager as it removes the geckodriver version dependency
			    	WebDriverManager.firefoxdriver().setup();
			    	FirefoxOptions opt = new FirefoxOptions();
			    	opt.setHeadless(true);
			    	driver = new FirefoxDriver(opt);
			    	break;
			     }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(15));
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get(url);
	}
}