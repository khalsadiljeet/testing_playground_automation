package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.base.TestBase;
import com.pages.Home;
import com.pages.Visibility;

/*This class file contains test cases related to Visibility page*/
public class VisibilityLinkTest extends TestBase {
	
	Home homePage;
	Visibility visibilityPage;
	
	@BeforeMethod
	public void setUp() throws FileNotFoundException, IOException {
		
		initialization();
		homePage = new Home(driver);
		visibilityPage = new Visibility(driver);
	}
	
	/*
	 * This test case clicks on Hide button on Visibility page & then checks for
	 * other buttons whether they are hidden or not
	 */
	@Test(priority=1)
	public void verifyButtonVisibility() throws InterruptedException{
		
		homePage.clickOnVisibilityLink();
		visibilityPage.clickHideButton();
		AssertJUnit.assertTrue(visibilityPage.verifyRemovedButtonIsHidden());
		AssertJUnit.assertTrue(visibilityPage.verifyZeroWidthButtonIsHidden());
		AssertJUnit.assertTrue(visibilityPage.verifyOpacityButtonIsHidden());
		AssertJUnit.assertTrue(visibilityPage.verifyVisibilityHiddenButtonIsHidden());
		AssertJUnit.assertTrue(visibilityPage.verifyDisplayNoneButtonIsHidden());
		AssertJUnit.assertTrue(visibilityPage.verifyOffscreenButtonIsHidden());
	}
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
}
