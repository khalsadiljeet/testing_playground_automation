package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.base.TestBase;
import com.pages.Home;
import com.pages.SampleApp;
import com.utils.ConfigManager;

/*This class file contains test cases related to Sample App page*/
public class SampleAppLinkTest extends TestBase {
	
	Home homePage;
	SampleApp sampleAppPage;
	
	@BeforeMethod
	public void setUp() throws FileNotFoundException, IOException {
		
		initialization();
		homePage = new Home(driver);
		sampleAppPage = new SampleApp(driver);
	}
	
	/*
	 * This test case enters username & password on Smaple App page & then verifies
	 * login status text
	 */
	@Test(priority=1)
	public void verifyLoginStatusAfterLogin() throws InterruptedException, IOException{
		
		String username = ConfigManager.getInstance().getString("UserName");
		String password = ConfigManager.getInstance().getString("Password");
		String expectedLoginStatusText = "Welcome, "+username+"!";
		String actualLoginStatusText;
		
		homePage.clickOnSampleAppLink();
		sampleAppPage.enterUserName(username);
		sampleAppPage.enterPassword(password);
		sampleAppPage.clickLoginButton();
		actualLoginStatusText = sampleAppPage.getLoginStatusText();
		
		AssertJUnit.assertEquals(expectedLoginStatusText, actualLoginStatusText);
	}
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
	
	

}
