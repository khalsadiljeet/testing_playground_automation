package tests;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.base.TestBase;
import com.pages.ClassAttribute;
import com.pages.Home;
import com.utils.ConfigManager;

/*This class file contains test cases related to Class Attribute page*/
public class ClassAttributeLinkTest extends TestBase {
	
	Home homePage;
	ClassAttribute classAttributePage;
	
	@BeforeMethod
	public void setUp() throws FileNotFoundException, IOException {
		
		initialization();
		homePage = new Home(driver);
		classAttributePage = new ClassAttribute(driver);
	}
	
	
	/*
	 * This test case clicks blue button on Class Attribute page & then verifies
	 * text on alert & clicks Ok button on alert
	 */
	@Test(priority=1)
	public void verifyButtonClickAndAcceptPopUp() throws InterruptedException, IOException{
		String expectedAlertText = ConfigManager.getInstance().getString("AlertText");
		String actualAlertText;
		homePage.clickOnClassAttributeLink();
		classAttributePage.clickButton();
		classAttributePage.switchToAlert();
		actualAlertText = classAttributePage.getAlertText();
		AssertJUnit.assertEquals(actualAlertText, expectedAlertText);
		classAttributePage.clickOkOnAlert();
	}
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
}
