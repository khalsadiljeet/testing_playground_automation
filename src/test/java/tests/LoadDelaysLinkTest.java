package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.base.TestBase;
import com.pages.Home;
import com.pages.LoadDelays;

/*This class file contains test cases related to Load Delays page*/
public class LoadDelaysLinkTest extends TestBase {
	
	Home homePage;
	LoadDelays loadDelaysPage;
	
	@BeforeMethod
	public void setUp() throws IOException {
		
		initialization();
		homePage = new Home(driver);
		loadDelaysPage = new LoadDelays(driver);
	}
	
	/*
	 * This test case waits for page load before clicking onto Button on Load Delays
	 * page
	 */
	@Test(priority=1)
	public void verifyPageLoadThenClickButton() throws InterruptedException{
		
		homePage.clickOnLoadDelayLink();
		homePage.waitForPageLoad();
		loadDelaysPage.clickButtonAppearingAfterDelay();
	}
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
	
	

}
