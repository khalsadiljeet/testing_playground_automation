Given:
1. Environment and specs:
   http://www.uitestingplayground.com/ (UI)
2. Scripting language used: Java
3. Testing tool: Eclipse
4. CI platform: Gitlab

Scenarios:
 1. User clicks on Visibility link from Home page then clicks on Hide button & verifies whether other buttons are hidden or not.
 2. User clicks on Load Delays link from Home page & is able to wait for page to load completely before clicking onto the button.
 3. User clicks on Class Attribute link from Home page then clicks on Button with blue color, verifies alert text & clicks Ok
    on alert.
 4. User clicks on Sample App link from Home page, enters UserName & password & clicks Log In button then verifies LoginStatus text.

a. How to run test cases locally?
 1. testng.xml file -> Right click on testng.xml file & then Run As -> TestNG Suite. This will run all the test cases
    at once
 2. Click on Run button available just above individual test methods
 
b. How to run the test in a CI/CD pipeline?
   1. Navigate to repository on gitlab.com "https://gitlab.com/khalsadiljeet/testing_playground_automation", 
       click CI/CD -> Pipelines & then click Run Pipeline button. This will run the tests in pipeline.
   2. On every change (commit & push) to repository, automatic pipeline will be triggered which will run tests.
   
c. Link to results in Calliope.pro
   https://app.calliope.pro/reports/146698
   
d. Describe one improvement point and one new feature for the Calliope.pro platform:                                                                
   Improvement point: Looks like there is no support for html reports for now, supporting html reports as well can be a improvement
   point.                                                                                                                      
   
   New feature Suggestion: There can be a feature which allows team members to post comment(s) onto the Reports. These 
   comments could be used for providing any analysis or tagging someone from team to draw their attention or for any failed
   test case if someone knows there is already a defect for it then defect id can be mentioned in the comments etc.

e. What you used to select the scenarios, what was your approach?                                                          
   Approach was to cover scenarios where usually challenges are present like identifying hidden elements/elements with
   dynamic attributes, page load related issues.

f. Why are they the most important?                                          
   Looking application under test, i feel below are the most common user cases:
   1. verifyButtonVisibility() -> Dealing with hidden elements on UI can be challenging, this test case demonstrates how
      to handle hidden elements i.e. buttons in this case.
   2. verifyPageLoadThenClickButton() -> Delayed Page load is very common thing which makes automated test cases flaky, 
      this test case shows how to wait for a page to load & then continue for execution.
   3. verifyButtonClickAndAcceptPopUp() -> Class is very common attribute for an element but when there is more than one class
      reference then xpath relying on class must be well formed. This test case locates such element using correct variant of xpath.
   4. verifyLoginStatusAfterLogin() -> LoginStatus text in this case is dynamic based on Username, this test case takes care 
      of dynamic text.
   
g. What could be the next steps to your project?                                                                                
   According to Testing pyramid, evaluate which scenarios can be part of API/UI, then cover as much scenarios to increase 
   the coverage including negative scenarios:
   1. Add remaining page objects for remaining screens of the application & cover respective scenarios.
   2. Add a function to take screenshots during the test case execution.
   3. Reporting can be improved like which has more details. Also, we can try sending report over email after every execution.
   4. In case regression suites gets heavy in future, we can provide support for parallel execution of test cases to reduce the
      overall execution time.
   5. Implement loggers.
   6. Test cases execution are flaky on chrome browser, need to investigate.

